;(async function () {
  'use strict'

  let filters = []

  const container = document.querySelector('.gallery-container')
  if (!container) {
    return
  }

  const items = container.querySelectorAll('.gallery-item')

  for (const item of items) {
    const image = item.querySelector('.gallery-image')
    const data = image.childNodes.item(0)
    const project = image.dataset.project

    try {
      const metadata = await getMetadata(project)
      const preview = metadata.body.querySelector('.preview')
      if (preview) {
        const img = preview.querySelector('img')
        const href = new URL(img.getAttribute('src'), metadata.href)
        data.src = href.href
        const tagsHtml = item.querySelector('.gallery-tags')
        const tags = getMeta('keywords', metadata.head).split(',')
        item.dataset.tags = tags
        tags.forEach((tag) => {
          const tagHtml = tagsHtml.appendChild(document.createElement('span'))
          tagHtml.innerText = tag.trim()
          tagHtml.addEventListener('click', filter)
        })
      } else {
        console.log(`project ${project} has no preview`)
        item.remove()
      }
    } catch (e) {
      // To leave other items on the screen
      item.remove()
      console.error(project)
      console.error(e)
    }
  }

  const images = document.querySelectorAll('.miniature')
  for (const image of images) {
    const img = image.querySelector('img')
    const parent = img.parentElement
    img.remove()

    const src = img.src
    const href = src.split('.')
    const ext = href.pop()

    const link = document.createElement('a')
    link.href = href.join('.') + '_full.' + ext
    link.target = 'blank'

    link.appendChild(img)
    parent.appendChild(link)
  }

  var sidebar = document.querySelector('aside.tags.sidebar')
  if (!sidebar) return

  var menu = sidebar.querySelector('.tags-menu')
  if (!menu) (menu = document.createElement('div')).className = 'tags-menu'

  var title = document.createElement('h3')
  title.textContent = sidebar.dataset.title || 'Tags'
  menu.appendChild(title)

  var list = menu.dataset.tags.split(',').reduce(function (accum, tag) {
    var tagItem = document.createElement('span')
    tagItem.innerText = tag.trim()
    tagItem.addEventListener('click', filter)
    accum.appendChild(tagItem)
    return accum
  }, document.createElement('p'))
  list.classList.add('project-tags')
  menu.appendChild(list)

  function filter (event) {
    console.log(event.target)
    const filter = event.target.innerText
    const remove = event.target.classList.contains('tag-selected')
    const container = document.querySelector('.gallery-container')
    const items = container.querySelectorAll('.gallery-item')
    updateFilters(filter, remove)

    if (remove) {
      event.target.classList.remove('tag-selected')
    } else {
      event.target.classList.add('tag-selected')
    }

    for (const item of items) {
      const tags = item.querySelector('.gallery-tags').childNodes
      let isFilter = false
      let numberOfTags = 0
      for (const tag of tags) {
        if (tag.innerText === filter) {
          if (remove) {
            tag.classList.remove('tag-selected')
          } else {
            tag.classList.add('tag-selected')
            isFilter = true
            numberOfTags++
          }
        } else {
          if (remove) {
            isFilter = isFilter || (tag.classList !== undefined && tag.classList.contains('tag-selected'))
          }
          if (tag.classList !== undefined && tag.classList.contains('tag-selected')) {
            numberOfTags++
          }
        }
      }
      if ((isFilter && filters.length === numberOfTags) || filters.length === 0) {
        item.classList.remove('gallery-item-masked')
      } else {
        item.classList.add('gallery-item-masked')
      }
    }
  }

  function updateFilters (filter, remove) {
    if (remove) {
      filters = filters.filter(function (value, index, arr) {
        return value !== filter
      })
    } else {
      filters.push(filter)
    }
  }

  async function getMetadata (relative) {
    const url = new URL(relative, window.location.href)
    console.log(url.href)
    const response = await fetch(url)
    if (response.status === 200) {
      const parser = new DOMParser()
      const doc = parser.parseFromString(await response.text(), 'text/html')
      return {
        href: url.href,
        body: doc.body,
        head: doc.head,
      }
    } else {
      throw new Error(response.status)
    }
  }

  function getMeta (metaName, element) {
    const metas = element.getElementsByTagName('meta')

    for (let i = 0; i < metas.length; i++) {
      if (metas[i].getAttribute('name') === metaName) {
        return metas[i].getAttribute('content')
      }
    }
    return ''
  }
})()
